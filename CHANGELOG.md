# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.3.0] - 2020-06-25
### Added
- *ReturnMessageAdapter* allows for non-inheritance-based pre-addressed return messages (similar to AF Self-Addressed Msg, but without the liabilities).

## [0.2.1] - 2020-06-20
### Deprecated
- Marked 'Send Immediate Stop Msg' for removal because it did not actually work. Use Send Priority Stop Msg instead with a priority of 'High'.
### Removed
- GPM 'Dev' dependency on @cs/ci

## [0.2.0] - 2020-02-07
### Added
- PriorityStopMessage allows stopping with L/M/H priority level via 'Send Priority Stop Msg' or Critical priority using 'Send Immediate Stop Msg'.

## [0.1.0] - 2019-04-10
### Added
- Everything (initial publication includes MessageCycle and RoundTripMessage)